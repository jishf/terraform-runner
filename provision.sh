#!/bin/bash

mkdir /tmp/terraform 
cd /tmp/terraform || exit

microdnf update
microdnf install jq unzip git bind-utils

curl -O -sS -L https://releases.hashicorp.com/terraform/"${TERRAFORM_VERSION}"/terraform_"${TERRAFORM_VERSION}"_linux_amd64.zip 
curl -O -sS -L https://github.com/gruntwork-io/terragrunt/releases/download/"${TERRAGRUNT_VERSION}"/terragrunt_linux_amd64 

mv terragrunt_linux_amd64 /usr/local/bin/terragrunt 
chmod +x /usr/local/bin/terragrunt 

unzip terraform_"${TERRAFORM_VERSION}"_linux_amd64.zip 
mv terraform /usr/local/bin

cd /tmp/ || exit
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install

microdnf remove unzip

rm -rf /var/cache/yum
rm -rf /tmp/terraform
rm -rf /tmp/awscliv2.zip
